import { createStore } from "solid-js/store";
import type { StoreNode, Store, SetStoreFunction } from "solid-js/store";

// function createStore<T extends StoreNode>(
//   state: T | Store<T>
// ): [get: Store<T>, set: SetStoreFunction<T>]

// Modal State
export const [modalState, setModalState] = createStore({
  isModalOpen: false,
});

// State
const columnIds = genColumnIds(3);
const taskIds = genColumnIds(4);

export const [state, setState] = createStore({
  [columnIds[0]]: {
    title: "Todo",
    tasks: {
      [taskIds[0]]: {
        taskId: [taskIds[0]],
        columnId: columnIds[0],
        title: "Wash dishes",
        description: "Do you really need one?",
      },
      [taskIds[1]]: {
        taskId: [taskIds[1]],
        columnId: columnIds[0],
        title: "Throw out trash",
        description: "Do you really need one?",
      },
      [taskIds[2]]: {
        taskId: [taskIds[2]],
        columnId: columnIds[0],
        title: "Clean fridge",
        description: "Do you really need one?",
      },
    },
  },
  [columnIds[1]]: {
    title: "Doing",
    tasks: {
      [taskIds[3]]: {
        taskId: [taskIds[3]],
        columnId: columnIds[1],
        title: "Program site",
        description: "Do you really need one?",
      },
    },
  },
});

function genColumnIds(howMany: number): string[] {
  if (howMany <= 0)
    throw Error("Number for columns to generate must be positive");
  const ids = [];
  while (howMany > 0) {
    ids.push(Math.random().toString());
    howMany--;
  }
  return ids;
}
