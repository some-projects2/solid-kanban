import { createSignal } from "solid-js";
import { unwrap, produce, modifyMutable } from "solid-js/store";
import { leading, debounce } from "@solid-primitives/scheduled";

import ColumnModel from "../models/Column.model";
import Task from "./Task";
import classes from "./Column.module.css";
import AddTask from "./AddTask";
import { state, setState } from "../store/store";

function Column(props: { column: ColumnModel }) {
  const [dragOver, setDragOver] = createSignal(false);

  const dragOverHandler = (e: DragEvent) => {
    e.preventDefault();
    leading(
      debounce,
      (e: DragEvent) => {
        if (
          !dragOver() &&
          e.dataTransfer &&
          e.dataTransfer.types[0] === "text/plain"
        ) {
          setDragOver(true);
        }
      },
      200
    )(e);
  };

  // const dragOverHandler = (e: DragEvent) => {
  //   e.preventDefault();
  // };

  const dropHandler = (e: DragEvent) => {
    console.log("dropHandler3 state", unwrap(state));
    e.preventDefault();
    const [fromColumnId, taskId] = e
      .dataTransfer!.getData("text/plain")
      .split("-");
    if (fromColumnId === props.column.id) return; // drop on current does nothing
    setState((state: any) => {
      modifyMutable(
        state,
        produce((s) => {
          s[props.column.id!].tasks[taskId] = {
            ...s[fromColumnId].tasks[taskId],
            columnId: props.column.id,
          };
          s[fromColumnId].tasks[taskId] = undefined;
        })
      );
    });
  };

  const dragLeaveHandler = (e: Event) => {
    if (dragOver()) {
      setDragOver(false);
    }
  };

  return (
    <div
      class={`${classes.column} shadow hover:shadow-md ${
        dragOver() ? classes.dragOver : null
      }`}
      onDragOver={dragOverHandler}
      onDrop={dropHandler}
      onDragLeave={dragLeaveHandler}
    >
      <h3 class={`font-bold`}>{props.column.title}</h3>
      <div class={classes.tasks}>
        {Object.entries(props.column.tasks).map(([id, t]) => (
          <Task task={{ ...t }} />
        ))}
        <AddTask columnId={props.column.id} />
      </div>
    </div>
  );
}

export default Column;
