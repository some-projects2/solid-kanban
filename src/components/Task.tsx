import TaskModel from "../models/Task.model";
import { Draggable } from "../models/Draggable.model";

import classes from "./Task.module.css";

function Task(props: { task: TaskModel }) {
  console.log("props.task", props.task.columnId);
  const dragStartHandler = (e: DragEvent) => {
    console.log("dragStart taskId");
    e.dataTransfer!.setData(
      "text/plain",
      `${props.task.columnId}-${props.task.taskId![0]}`
    );
    e.dataTransfer!.effectAllowed = "move";
  };
  const dragEndHandler = () => {
    console.log("dragEnd");
  };

  return (
    <div
      class={`${classes.task} px-6 py-2 rounded cursor-pointer hover:shadow-sm`}
      draggable={true}
      onDragStart={dragStartHandler}
      onDragEnd={dragEndHandler}
    >
      {props.task.title}
    </div>
  );
}

export default Task;
