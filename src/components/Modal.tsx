import classes from "./Modal.module.css";
import { state, setState, setModalState } from "../store/store";
import { Task } from "../models/Task.model";

interface Props {
  columnId: string;
}

function Modal(props: Props) {
  let title: HTMLInputElement;
  let description: HTMLTextAreaElement;
  let column: HTMLSelectElement;

  const closeHandler = () => {
    setModalState((state) => ({
      isModalOpen: false,
    }));
  };

  const saveHandler = () => {
    const tags = [...description.value.matchAll(/#[a-zA-Z]*/g)]
      .flat()
      .map((tag) => tag.replace("#", ""));
    // console.log("title", title.value);
    // console.log("description", description.value);
    // console.log("column", column.value);
    // console.log("tags", tags);
    const taskId = Math.random().toString();
    const task: Task = {
      taskId,
      columnId: props.columnId,
      title: title.value,
      description: description.value,
      tags,
    };

    console.log("state before", state);
    setState((state) => {
      console.log("state is", state);
      console.log("props.columnId is", props);
      state[props.columnId].tasks[taskId] = task;
      return { ...state };
    });
    console.log("state", state);
  };

  return (
    <div class={classes.modalContainer}>
      <div
        class={`${classes.modal} relative p-2 w-full max-w-2xl md:h-auto shadow
        rounded`}
      >
        <button class={`${classes.closeBtn} btn`} onClick={closeHandler}>
          &#215;
        </button>
        <form class={`${classes.form} px-2`}>
          <label htmlFor="title" class={`font-bold`}>
            Title
          </label>
          <input
            type="text"
            name="title"
            class={`${classes.titleInput} border-[1px]  border-gray-800 rounded py-1 px-2`}
            ref={title}
          />
          <label htmlFor="description" class={`font-bold`}>
            Description
          </label>
          <textarea
            name="description"
            rows="4"
            class={`${classes.description}  border-[1px] border-gray-800 rounded py-1 px-2`}
            ref={description}
          ></textarea>
        </form>
        <div class={classes.columnAndActions}>
          <select
            name="column"
            class="underline underline-offset-8"
            ref={column}
          >
            <option value="todo">Todo</option>
            <option value="doing">Doing</option>
          </select>
          <button
            class="py-2 px-4 rounded-lg border-1 border-gray-800 drop-shadow-md hover:drop-shadow-xl"
            onClick={closeHandler}
          >
            Cancel
          </button>
          <button
            class="bg-gray-800 text-white py-2 px-4 rounded-lg border-1 border-gray-800 drop-shadow-md hover:drop-shadow-xl"
            onClick={saveHandler}
          >
            Save
          </button>
        </div>
      </div>
    </div>
  );
}

export default Modal;
