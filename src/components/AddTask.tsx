import Modal from "./Modal";
import { setState, modalState, setModalState } from "../store/store";

interface Props {
  columnId: string;
}

function AddTask(props: Props) {
  const modalHandler = () => {
    console.log("modalHandler", modalState);
    setModalState((state) => ({ isModalOpen: !state.isModalOpen }));
  };

  return (
    <>
      <div class="text-center -ml-2 cursor-pointer" onClick={modalHandler}>
        + Add Task
      </div>
      {modalState.isModalOpen && <Modal columnId={props.columnId} />}
    </>
  );
}

export default AddTask;
