import { For, createMemo } from "solid-js";
import type { Component } from "solid-js";

import Column from "./components/Column";
import AddColumn from "./components/AddColumn";
import classes from "./App.module.css";
import { state } from "./store/store";

const App: Component = () => {
  const columns = createMemo(() => Object.entries(state));
  console.log("state is", state);
  console.log("columns", columns);
  return (
    <div class={`${classes.app}`}>
      <For each={columns()}>
        {([id, col]) => <Column column={{ id, ...col }} />}
      </For>
      {/* {Object.entries(state).map(([id, col]) => (
        <Column column={{ id, ...col }} />
      ))} */}
      <AddColumn />
    </div>
  );
};

export default App;
