export default interface Task {
  taskId?: string;
  columnId: string;
  title: string;
  description?: string;
  tags?: string[];
}
