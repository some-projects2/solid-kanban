import Task from "./Task.model";

export default interface Column {
  id?: string;
  title: string;
  tasks: { string: Task };
}
